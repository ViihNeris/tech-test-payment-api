using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly PaymentContext _context;

        public SaleController(PaymentContext context)
        {
            _context = context;
            Sale SaleEntity = new Sale();
            var StatusSale = SaleEntity.StatusSale;
        }

        [HttpPost]
        public IActionResult Create(Sale sale)
        {
            _context.Add(sale);
            _context.SaveChanges();
            return CreatedAtAction(nameof(GetById), new { id = sale.Id }, sale);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var sale = _context.Sales.Find(id);
            if (sale == null) return NotFound();
            return Ok(sale);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStatus(int id, Sale sale){
            var saleDatabase = _context.Sales.Find(id);
            if(saleDatabase == null) return NotFound();


            // if(saleDatabase.StatusSale.ToString().Length > 4){
            //     return BadRequest();
            // }
            
            saleDatabase.StatusSale = sale.StatusSale;

            _context.Sales.Update(saleDatabase);
            _context.SaveChanges();
            return Ok(saleDatabase);
        }
    }
}