using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public class Sale
    {
        /*Uma venda contém informação sobre o 
        vendedor que a efetivou, data, identificador do pedido 
        e os itens que foram vendidos;*/
        public int Id { get; set; }
        public int IdSeller { get; set; }
        public DateTime DateSale { get; set; }
        public string SoldItems { get; set; }
        public EnumStatusSale StatusSale { get; set; }
    }
}