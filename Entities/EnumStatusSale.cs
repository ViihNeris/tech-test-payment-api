namespace tech_test_payment_api.Entities
{
    public enum EnumStatusSale
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Enviado_Para_Transportadora,
        Entregue,
        Cancelada
    }
}